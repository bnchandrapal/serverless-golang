package main

import (
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler() (events.APIGatewayProxyResponse, error) {
	fmt.Println("[+] Just executed the sample function")
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       "Chandrapal Badshah",
		Headers: map[string]string{
			"Content-Type": "text/html",
		},
	}, nil
}

func main() {
	lambda.Start(handler)
}
