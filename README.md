# Serverless Golang

Learning Serverless Framework + Golang to create awesomeness

### Programs

01-sample-app : Sample Go application which returns "Chandrapal Badshah" (in text/html content type)

### What did I learn ?

- Service function should only contain alphabets and hypens (no integers)
- The **package name should always be `main`**
- Whatever is sent as response should get through the API gateway to get to the client. Else theres an internal error.
- Serverless Framework by default provides access to AWS Log writing. Any printed message gets stored in logs.